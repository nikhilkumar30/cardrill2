// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

// Create function sortCarModelsAlphabetically and initialise carModels to empty array
function sortCarModelsAlphabetically(inventory) {
  const carModels = [];

  let result;
  result = inventory.filter((elemnts) => {
    return carModels.push(elemnts.car_model);
  });

  carModels.sort();

  console.log(carModels);
}

module.exports = sortCarModelsAlphabetically;
