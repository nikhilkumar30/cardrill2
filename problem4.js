// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

// Create function getAllCarYears and create empty array with name carYears
function getAllCarYears(inventory) {
  const carYears = [];

  let result;

  result = inventory.filter((elements) => {
    return carYears.push(elements.car_year);
  });

  return carYears
  // console.log(carYears);
}

// Export the function
module.exports = getAllCarYears;
