// Here we import the car data array and function for problem1
const inventory = require("../carData");
const findCarById = require("../problem1");

// Here we call the function fincdCarById
const car33 = findCarById(inventory, 33);

// Here is condition for checking id
if (car33) {
  console.log(
    `Car 33 is a ${car33.car_year} ${car33.car_make} ${car33.car_model}`
  );
} else {
  console.log("Car with id 33 not found.");
}
