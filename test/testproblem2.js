//import carData and function from problem2
const inventory = require("../carData");
const getLastCar = require("../problem2");

const lastCar = getLastCar(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
