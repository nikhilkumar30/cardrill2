// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

// Function to filter and log BMW and Audi cars
function filterBMWAndAudi(inventory) {
  let result;

  result = inventory.filter((element) => {
    return element.car_make == "BMW" || element.car_make == "Audi";
  });

  console.log("BMW and Audi cars:");
  console.log(result);

  return result;
}

module.exports = filterBMWAndAudi;
