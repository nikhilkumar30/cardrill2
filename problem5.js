// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


// import problem 4
const getAllCarYears=require('../CarDrill2/problem4')

// Function to find and log older cars from the year 2000 and their count no
function findOlderCars(inventory) {
    let olderCars = [];
  
    // Filter cars older than the year 2000
    CarYear=getAllCarYears(inventory);
    olderCars=CarYear.filter((elements)=>{
         return elements<2000;
    })
    
    console.log(olderCars);
    console.log("Number of older cars:", olderCars.length);
    
  }
  
  // Export the function
  module.exports= findOlderCars;
  